# Use the official lightweight Python image.
# https://hub.docker.com/_/python
FROM python:3.11-slim

# Set the working directory to /app
WORKDIR /app

# Copy the requirements file into the image
COPY requirements.txt .

# Install any dependencies
RUN python -m pip install -r requirements.txt

# Copy the rest of the application code
COPY . .

# Command to run the application
CMD ["python", "-m", "src.service.app"]
