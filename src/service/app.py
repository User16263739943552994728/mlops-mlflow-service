import os
from contextlib import asynccontextmanager
from functools import lru_cache
from typing import Annotated

import mlflow
import uvicorn
from dotenv import load_dotenv
from fastapi import Depends, FastAPI
from pydantic_settings import BaseSettings

from src.service.prediction_service import PredictionService
from src.service.schemas import PredictionData, PredictionResponse

load_dotenv()


class Settings(BaseSettings):
    embedding_run_id: str = os.getenv("DEFAULT_EMBEDDING_RUN_ID", default="")
    lr_run_id: str = os.getenv("DEFAULT_MODEL_RUN_ID", default="")
    mlflow_tracking_uri: str = os.getenv("MLFLOW_TRACKING_URI", default="")
    port: int = 8000


@asynccontextmanager
async def startup(_: FastAPI):
    mlflow.set_tracking_uri(settings.mlflow_tracking_uri)
    yield


settings = Settings()
app = FastAPI(lifespan=startup)


@lru_cache
def get_model() -> PredictionService:
    return PredictionService(embedding_run_id=settings.embedding_run_id, lr_run_id=settings.lr_run_id)


@app.post("/predict_salary")
async def predict(data: PredictionData, model: Annotated[PredictionService, Depends(get_model)]) -> PredictionResponse:
    return PredictionResponse(prediction=model.predict(data))


if __name__ == "__main__":
    uvicorn.run(app, host="0.0.0.0", port=settings.port)
