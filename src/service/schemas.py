from typing import Literal

from pydantic import BaseModel, Field

EmbeddingType = Literal["fasttext", "huggingface"]


class PredictionData(BaseModel):
    custom_position: str = Field(..., description="Work position", examples=["Programmer", "Driver"])


class PredictionResponse(BaseModel):
    prediction: float = Field(..., description="Predicted salary")
