import mlflow
import numpy as np
import pandas as pd

from src.service.schemas import PredictionData


class PredictionService:
    def __init__(self, embedding_run_id: str, lr_run_id: str):
        self.embedding_model = mlflow.pyfunc.load_model(f"runs:/{embedding_run_id}/embedding_model")
        self.model = mlflow.pyfunc.load_model(f"runs:/{lr_run_id}/lr_model")

    def calculate_embeddings(self, data: pd.Series) -> np.ndarray:
        embedding = self.embedding_model.predict([data.custom_position])
        return embedding

    def predict(self, data: PredictionData) -> float:
        series_raw = pd.Series(data.model_dump())
        embedding = self.calculate_embeddings(series_raw)
        prediction = self.model.predict(embedding)
        return prediction
