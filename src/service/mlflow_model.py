import pickle

import mlflow.pyfunc
import numpy as np
import pandas as pd


class LinearRegressionMlflowModel(mlflow.pyfunc.PythonModel):
    def __init__(self, model=None):
        self.model = model

    def predict(
        self,
        context,  # pylint: disable=unused-argument
        model_input,
    ):
        emb = np.array(model_input)
        return pd.Series(self.model.predict(emb))

    def load_context(self, context):
        with open(context.artifacts["lr_model"], "rb") as f:
            self.model = pickle.load(f)


class EmbeddingMlflowModel(mlflow.pyfunc.PythonModel):
    def __init__(self, model=None):
        self.model = model

    def predict(
        self,
        context,  # pylint: disable=unused-argument
        model_input,
    ):
        text = model_input
        return self.model.generate(text)

    def load_context(self, context):
        with open(context.artifacts["embedding_model"], "rb") as f:
            self.model = pickle.load(f)
