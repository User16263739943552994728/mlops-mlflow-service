import json

import requests

url = "http://localhost:8000/predict_salary"
data = {"custom_position": "Programmer"}
response = requests.post(url, data=json.dumps(data))
print(response.json())
